# TOKENIZER-CREDIT-CARDS

Tokenizer es una aplicación que permite almacenar la información de las tarjetas de crédito con un token generado por la aplicación.   

## REQUERIMIENTOS TÉCNICOS

- [NodeJs](https://nodejs.org/en/)  v.16.x
- [Docker](https://docs.docker.com/get-docker/)
- [SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)


## GETTING STARTED
- Clonar el repositorio publico usando
```
git clone https://gitlab.com/jcjimenezvar/tokenizer-credit-cards.git
```
- Luego ubicarse en la raiz del proyecto y ejecutar
```
cd tokenizer-credit-cards
```
- Instalar las dependencias
```
npm i
```
- Crear la imagen y container de docker para mongo:
```
docker run -d -p 27017:27017 --name tokenizer mongo:latest 
```
- Ejecutar el programa:
```
sam local start-api  
```
El comando anterior levantara una instancia de `AWS API-GATEWAY` que nos permite utilizar los endpoint expuestos `POST /v2/tokens` y `GET /v2/get-credit-card`. Evidenciaremos que la aplicación corre de forma correcta al visualizar lo siguiente
```
Mounting tokenizer at http://127.0.0.1:3000/v2/get-credit-card [GET]
Mounting tokenizer at http://127.0.0.1:3000/v2/tokens [POST]
You can now browse to the above endpoints to invoke your functions. You do not need to restart/reload SAM CLI while working on your functions, changes will be reflected instantly/automatically. You only need to restart SAM CLI if you update your AWS SAM template
2022-10-07 17:29:34  * Running on http://127.0.0.1:3000/ (Press CTRL+C to quit)

```

Para consumir los endpoints es necesario relizar las peticiones peticiones utilizando curl o importando los siguientes request en postman-> import -> Raw text
```
curl --location --request POST 'http://localhost:3000/v2/tokens' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "jjimenezv24@gmail.com",
    "card_number": "5555555555554444",
    "cvv": "123",
    "expiration_year": "2025",
    "expiration_month": "01"
}'

curl --location --request GET 'http://localhost:3000/v2/get-credit-card' \
--header 'Authorization: Bearer TE238sBQpWv692sE'
```