//import { ICreditCardInput, ICreditCardOuput } from "../db/models/creditCard";

import { ICreditCardAttributes } from "../db/models/creditCard";

export interface ICreditCardService {
  create(creditCard: any): Promise<any>;
  get(token: string): Promise<ICreditCardAttributes | null>;
}
