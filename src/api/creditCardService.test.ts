import { CreditCardRepository } from "../db/dal/credit-card/creditCard";
import { ICreditCardAttributes } from "../db/models/creditCard";
import { CreditCardService } from "./creditCardService";

jest.mock('mongoose')

describe("CreditCardService", () => {
  beforeEach(() => {
    process.env.PORT = "3000";
    process.env.DB_NAME = "tokenizer";
    process.env.DB_USER = "admin";
    process.env.DB_HOST = "172.17.0.1";
    process.env.DB_PORT = "27017";
    process.env.DB_PASSWORD = "admin";
    process.env.NODE_ENV = "development";
  });

  const mockCreditCard: ICreditCardAttributes = {
    card_number: "123445666",
    expiration_month: "12",
    email: "correo@correo.com",
    expiration_year: "2028"
  };
  const creditCard = new CreditCardService();
  const creditCardService = new CreditCardService();
  const creditCardRepository = new CreditCardRepository();
  test("CreditCardService, instance is created correctly", async () => {
    expect(creditCard).toBeDefined();
  });

  test("CreditCardService, create function returns credit card information", async () => {
    jest.spyOn(CreditCardRepository.prototype, 'createCreditCard').mockImplementationOnce(()=> Promise.resolve(mockCreditCard))
    const result = await creditCard.create(mockCreditCard)
    expect(result).toBeDefined();
  });

  test("CreditCardService, create function returns credit card information", async () => {
    jest.spyOn(CreditCardRepository.prototype, 'getCreditCard').mockImplementationOnce(()=> Promise.resolve(mockCreditCard))
    const result = await creditCard.get('token')
    expect(result).toBeDefined();
  });
});
