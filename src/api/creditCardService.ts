import { ICreditCardService } from ".";
import { CreditCardRepository } from "../db/dal/credit-card/creditCard";
import { ICreditCardAttributes } from "../db/models/creditCard";

export class CreditCardService implements ICreditCardService {
  public create(creditCard: ICreditCardAttributes): Promise<any> {
    return new CreditCardRepository().createCreditCard(creditCard);
  }

  public get(token: string): Promise<ICreditCardAttributes | null> {
    return new CreditCardRepository().getCreditCard(token);
  }
}
