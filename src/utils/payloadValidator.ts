import joi from 'joi';
import { ICreditCardAttributes } from '../db/models/creditCard';

export class PayloadValidator {
  public static validate(body: ICreditCardAttributes | string, schema: joi.Schema): void {
    const result: joi.ValidationResult = schema.validate(body, { abortEarly: false });
    if (result.error) {
      throw {
        statusCode: 400,
        message: 'Invalid data on request',
        data: result.error.details
      };
    }
  }
}
