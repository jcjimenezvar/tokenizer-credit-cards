import { ICreditCardAttributes } from "../db/models/creditCard";
import { PayloadValidator } from "./payloadValidator";
import { cardSchema, getCardSchema } from "./schemas";

describe("PayloadValidator", () => {
  const mockCreditCard: ICreditCardAttributes = {
    card_number: "5555555555554444",
    expiration_month: "12",
    email: "correo@correo.com",
    expiration_year: "2028",
    cvv: "122",
  };
  test("PayloadValidator credit card payload is valid", () => {
    const result = PayloadValidator.validate(mockCreditCard, cardSchema);
    expect(result).toBeUndefined();
  });

  test("PayloadValidator credit card payload throws an error", () => {
    mockCreditCard.card_number = "123445555";
    try {
      const result = PayloadValidator.validate(mockCreditCard, cardSchema);
      expect(result).toBeUndefined();
    } catch (error) {
      expect(error).toBeDefined();
    }
  });
});
