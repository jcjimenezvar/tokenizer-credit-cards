import joi from 'joi';

export const cardSchema = joi.object().keys({
  email: joi.string().email().required(),
  card_number: joi.string().min(13).max(16).creditCard().required(),
  cvv: joi.string().min(3).max(4).required(),
  expiration_year: joi.string().min(4).max(4).required(),
  expiration_month: joi.string().min(1).max(2).valid('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12').required()
}).required();

export const getCardSchema = joi.string().required();

