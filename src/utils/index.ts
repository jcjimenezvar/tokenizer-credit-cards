import { ICreditCardAttributes } from '../db/models/creditCard';

export interface IPayloadValidator {
  validate(body: ICreditCardAttributes): void;
}
