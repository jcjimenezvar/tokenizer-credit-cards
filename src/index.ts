import { run } from "./db/config";
import { SaveCreditCard } from "./router/post/saveCreditCard";
import { GetCreditCard } from "./router/get/getCreditCard";
import { PayloadValidator } from "./utils/payloadValidator";
import { getCardSchema, cardSchema } from "./utils/schemas";

export const handler = async (event: any, context: any): Promise<any> => {
  try {
    await run();
    switch (event.httpMethod) {
      case "GET":
        if (!event.headers["Authorization"]) {
          throw {
            statusCode: 400,
            message: "Invalid data on request: The generated token is required",
            data: {},
          };
        }
        PayloadValidator.validate(
          event.headers["Authorization"],
          getCardSchema
        );
        return new GetCreditCard(event.headers["Authorization"]).processData();
      case "POST":
        if (!event.body) {
          throw {
            statusCode: 400,
            message:
              "Invalid data on request: The credit card information is required",
            data: {},
          };
        }
        PayloadValidator.validate(JSON.parse(event.body), cardSchema);
        return new SaveCreditCard(JSON.parse(event.body)).processData();

      default:
        throw {
          statusCode: 404,
          message: "Method not implemented",
          data: {},
        };
    }
  } catch (error: any) {
    return {
      statusCode: error && error.statusCode ? error.statusCode : 500,
      body: JSON.stringify({ message: error.message, data: error.data }),
      headers: {
        "Content-Type": "application/json",
      },
      isBase64Encoded: false,
    };
  }
};
