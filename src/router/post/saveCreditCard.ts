import ShortUniqueId from "short-unique-id";
import { PayloadValidator } from "../../utils/payloadValidator";
import { CreditCardService } from "../../api/creditCardService";
import { ICreditCardAttributes } from "../../db/models/creditCard";
import { IProccesData } from "..";
import { cardSchema } from "../../utils/schemas";

export class SaveCreditCard implements IProccesData {
  private data: ICreditCardAttributes;
  constructor(data: ICreditCardAttributes) {
    this.data = data;
  }

  public processData = async () => {
    const shortUniqueId = new ShortUniqueId();
    this.data = { ...this.data, ...{ token: shortUniqueId.randomUUID(16) } };
    const creditCard = await new CreditCardService().create(this.data);

    if (!creditCard) {
      throw {
        statusCode: 400,
        message: "An occurred trying to save credit card information.",
      };
    }

    return {
      statusCode: 200,
      body: JSON.stringify({
        message: "Credit card information saved succesfully!",
        data: creditCard.id,
      }),
      headers: {
        "Content-Type": "application/json",
        "x-token": creditCard.token,
      },
      isBase64Encoded: false,
    };
  };
}
