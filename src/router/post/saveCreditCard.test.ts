import { SaveCreditCard } from "./saveCreditCard";
import { CreditCardService } from "../../api/creditCardService";
import { ICreditCardAttributes } from "../../db/models/creditCard";

jest.mock("../../api/creditCardService");

describe("SaveCreditCard", () => {
  beforeEach(() => {
    process.env.PORT = "3000";
    process.env.DB_NAME = "tokenizer";
    process.env.DB_USER = "admin";
    process.env.DB_HOST = "172.17.0.1";
    process.env.DB_PORT = "27017";
    process.env.DB_PASSWORD = "admin";
    process.env.NODE_ENV = "development";
  });

  const mockCreditCard: ICreditCardAttributes = {
    card_number: "123445666",
    expiration_month: "12",
    email: "correo@correo.com",
    expiration_year: "2028",
    token: "generatedAuthToken",
    cvv: "122"
  };
  const saveCreditCard = new SaveCreditCard(mockCreditCard);
  test("SaveCreditCard, instance is created correctly", async () => {
    expect(saveCreditCard).toBeDefined();
  });

  test("SaveCreditCard, save credit card information", async () => {
    jest
      .spyOn(CreditCardService.prototype, "create")
      .mockImplementationOnce(() => Promise.resolve(mockCreditCard));
    const result = await saveCreditCard.processData();
    expect(result).toBeDefined();
  });

  test("GetCreditCard, return null and throw an exception", async () => {
    jest
      .spyOn(CreditCardService.prototype, "create")
      .mockImplementationOnce(() => Promise.resolve(null));
    try {
      const result = await saveCreditCard.processData();
      expect(result).toBeUndefined();
    } catch (error) {
      expect(error).toBeDefined();
    }
  });
});
