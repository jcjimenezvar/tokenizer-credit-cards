import { GetCreditCard } from "./getCreditCard";
import { CreditCardService } from "../../api/creditCardService";
import { CreditCardRepository } from "../../db/dal/credit-card/creditCard";
import { ICreditCardAttributes } from "../../db/models/creditCard";

jest.mock("../../api/creditCardService");

describe("GetCreditCard", () => {
  beforeEach(() => {
    process.env.PORT = "3000";
    process.env.DB_NAME = "tokenizer";
    process.env.DB_USER = "admin";
    process.env.DB_HOST = "172.17.0.1";
    process.env.DB_PORT = "27017";
    process.env.DB_PASSWORD = "admin";
    process.env.NODE_ENV = "development";
  });

  const mockedToken = "Bearer token123444";
  const mockCreditCard: ICreditCardAttributes = {
    card_number: "123445666",
    expiration_month: "12",
    email: "correo@correo.com",
    expiration_year: "2028",
  };
  const getCreditCard = new GetCreditCard(mockedToken);
  test("GetCreditCard, instance is created correctly", async () => {
    expect(getCreditCard).toBeDefined();
  });

  test("GetCreditCard, returns correct credit card information", async () => {
    jest
      .spyOn(CreditCardService.prototype, "get")
      .mockImplementationOnce(() => Promise.resolve(mockCreditCard));
    const result = await getCreditCard.processData();
    expect(result).toBeDefined();
  });

  test("GetCreditCard, return null and throw an exception", async () => {
    jest
      .spyOn(CreditCardService.prototype, "get")
      .mockImplementationOnce(() => Promise.resolve(null));
    try {
      const result = await getCreditCard.processData();
      expect(result).toBeUndefined();
    } catch (error) {
      expect(error).toBeDefined();
    }
  });
});
