import { CreditCardService } from "../../api/creditCardService";
import { IProccesData } from "..";

export class GetCreditCard implements IProccesData {
  private token: string;
  constructor(token: string) {
    this.token = token;
  }

  public processData = async () => {
    const splitedToken: string = this.token.split("Bearer ")[1];
    const creditCard = await new CreditCardService().get(splitedToken);
    if (!creditCard) {
      throw {
        statusCode: 400,
        message: "An error occurred trying to obtain credit card information.",
      };
    }

    return {
      statusCode: 200,
      body: JSON.stringify({
        message: "The credit card information was obtained correctly!",
        data: creditCard,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      isBase64Encoded: false,
    };
  };
}
