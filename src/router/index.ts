import { IResponse } from "../db/models/intern/Response";

export interface IProccesData {
  processData(): Promise<IResponse| null>;
}