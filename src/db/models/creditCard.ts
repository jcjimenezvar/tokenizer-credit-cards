import { Schema, model, connect } from "mongoose";

export interface ICreditCardAttributes {
  card_number?: string;
  createdAt?: Date;
  cvv?: string;
  email?: string;
  expiration_month?: string;
  expiration_year?: string;
  deletedAt?: Date;
  _id?: any;
  token?: string;
  updatedAt?: Date;
}

const creditCard = new Schema<ICreditCardAttributes>(
  {
    card_number: { type: String, required: true },
    cvv: { type: String, required: true },
    email: { type: String, required: true },
    expiration_month: { type: String, required: true },
    expiration_year: { type: String, required: true },
    token: { type: String, required: true },
  },
  { timestamps: true }
);

export const CreditCard = model<ICreditCardAttributes>(
  "credit_card",
  creditCard
);
