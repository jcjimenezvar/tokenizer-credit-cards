import { connect } from "mongoose";

export const run = async () => {
  const dbName = process.env.DB_NAME as string;
  const dbHost = process.env.DB_HOST;
  const dbPort = process.env.DB_PORT;
  const mongoUri = `mongodb://${dbHost}:${dbPort}`;
  await connect(mongoUri, {
    dbName: dbName,
  });
};
