import { run} from './config'

jest.mock('mongoose')

describe("Config", ()=> {
  beforeEach(() => {
    process.env.PORT = "3000";
    process.env.DB_NAME = "tokenizer";
    process.env.DB_USER = "admin";
    process.env.DB_HOST = "172.17.0.1";
    process.env.DB_PORT = "27017";
    process.env.DB_PASSWORD = "admin";
    process.env.NODE_ENV = "development";
  });
  test("Config run fuction is called correctly", async ()=> {
    const response = await run()
    expect(response).toBeUndefined()

  })
})
