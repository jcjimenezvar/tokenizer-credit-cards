import { ICreditCardAttributes } from "../../models/creditCard";

export interface ICreditCardRepository {
  createCreditCard(creditCard: any): Promise<any>;
  getCreditCard(token: string): Promise<ICreditCardAttributes | null>;
}
