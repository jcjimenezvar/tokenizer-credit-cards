import { ICreditCardRepository } from ".";
import { CreditCard, ICreditCardAttributes } from "../../models/creditCard";

export class CreditCardRepository implements ICreditCardRepository {
  public async createCreditCard(
    creditCard: ICreditCardAttributes
  ): Promise<any> {
    return new CreditCard(creditCard).save();
  }

  public async getCreditCard(
    token: string
  ): Promise<ICreditCardAttributes | null> {
    return CreditCard.findOne(
      {
        token: token,
      },
      "card_number email expiration_month expiration_year"
    );
  }
}
