import { ICreditCardAttributes } from "../../models/creditCard";
import { CreditCardRepository } from "./creditCard";
import { CreditCard } from "../../models/creditCard";

describe("CreditCard Repository", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  const mockCreditCard: ICreditCardAttributes = {
    card_number: "123445666",
    expiration_month: "12",
    email: "correo@correo.com",
    expiration_year: "2028",
    cvv: "123",
    token: "token123",
  };
  const creditCardRepository = new CreditCardRepository();
  test("CreditCard Repository is instanced correctly", () => {
    expect(creditCardRepository).toBeDefined();
  });
  test("CreditCard Repository save return correct information", async () => {
    jest
      .spyOn(CreditCard.prototype, "save")
      .mockImplementationOnce(() => Promise.resolve(mockCreditCard));
    const data = await creditCardRepository.createCreditCard(mockCreditCard);
    expect(data).toBeDefined();
  });
  test("CreditCard Repository findOne return correct information", async () => {
    delete mockCreditCard.cvv
    CreditCard.findOne = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(mockCreditCard));
    const data = await creditCardRepository.getCreditCard("token123");
    expect(data).toBeDefined();
    expect(data?.cvv).toBeUndefined();
  });
});
